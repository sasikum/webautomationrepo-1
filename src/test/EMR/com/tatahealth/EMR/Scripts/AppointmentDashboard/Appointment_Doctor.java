package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.util.ArrayList;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Appointment_Doctor {

	ExtentTest logger;

	@BeforeClass(groups = { "Regression", "Appointment_Doctor" })
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTest();
		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "Appointment_Doctor" })
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	/***
	 * @author Bala Yaswanth
	 * @throws Exception Check Whether Date is defaulted to today in Appointments
	 *                   Page
	 */
	@Test(priority = 1, groups = { "Regression", "Appointment_Doctor" })
	public synchronized void APP_DOC_001() throws Exception {

		logger = Reports.extent.createTest("APP_DOC_001");
		AppointmentsPage appoiPage = new AppointmentsPage();
		String EMRDate = appoiPage.getCurrentDate(Login_Doctor.driver, logger).getText();
		Date date = new Date();
		String localDate = date.toString().substring(8, 10);
		Assert.assertEquals(localDate, EMRDate, "Date is not Correct. Please Check");

	}

	/***
	 * @author Bala Yaswanth
	 * @throws Exception Check Whether Date is defaulted to today in All Slots Page
	 */
	@Test(priority = 2, groups = { "Regression", "Appointment_Doctor" })
	public synchronized void APP_DOC_002() throws Exception {

		logger = Reports.extent.createTest("APP_DOC_002");

		AppointmentsPage appoiPage = new AppointmentsPage();

		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger),
				"Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);

		AllSlotsPage AsPage = new AllSlotsPage();
		String EMRDate = AsPage.getCurrentDate(Login_Doctor.driver, logger).getText();
		// String EMRDate = appoiPage.getCurrentDate(Login_Doctor.driver,
		// logger).getText();
		Date date = new Date();
		String localDate = date.toString().substring(8, 10);
		Assert.assertEquals(localDate, EMRDate, "Date is not Correct. Please Check");
		Web_GeneralFunctions.wait(5);
	}

	/***
	 * @author Bala Yaswanth
	 * @throws Exception Booking Appointment - Reschedule + Reschedule rescheduled
	 *                   appointment + cancel Appointment
	 */
	@Test(priority = 3, groups = { "Regression", "Appointment_Doctor" })
	public synchronized void APP_DOC_003() throws Exception {

		logger = Reports.extent.createTest("APP_DOC_003");

		AllSlotsPage ASpage = new AllSlotsPage();

		// Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		System.out.println(n);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId) - 2);

		// Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);

		// Rescheduling appointment to next slot
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				Login_Doctor.driver);
		Web_GeneralFunctions.wait(10);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// generalFunctions.click(ASpage.getButtonfromDropdown(n, 4,
		// Login_Doctor.driver, logger),"Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		// **Web_GeneralFunctions.click(ASpage.getReschedulefromDropdown(n,
		// Login_Doctor.driver, logger),"Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		// clicking on the getReschedulefromDropdownNum from drop down
		int num = ASpage.getReschedulefromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReschedulefromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
		// System.out.println(ASpage.getReschedulefromDropdown(num, Login_Doctor.driver,
		// logger));

		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Doctor.driver, logger),
				"Clicking to get timings in Reschedule Popup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n + 2), Login_Doctor.driver, logger),
				"Clicking to select next slot timings in Reschedule Popup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Doctor.driver, logger),
				"Clicking on Ok in Popup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Rescheduling the Rescheduled Appointment
		n = n + 1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				Login_Doctor.driver);

		Web_GeneralFunctions.wait(6);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// generalFunctions.click(ASpage.getButtonfromDropdown(n, 4,
		// Login_Doctor.driver, logger),"Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);
		// ** Web_GeneralFunctions.click(ASpage.getReschedulefromDropdown(n,
		// Login_Doctor.driver, logger),"Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getReschedulefromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReschedulefromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Doctor.driver, logger),
				"Clicking to get timings in Reschedule Popup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n + 3), Login_Doctor.driver, logger),
				"Clicking to select next slot timings in Reschedule Popup", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Doctor.driver, logger),
				"Clicking on Ok in Popup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(10);

		// Canceling the Rescheduled Appointment
		n = n + 1;

		num = ASpage.getAppointmentDropDownNum(Login_Doctor.driver, logger);

		// **Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n,
		// Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown1(num, Login_Doctor.driver, logger),
				Login_Doctor.driver);

		// **Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n,
		// Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment",
		// Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown1(num, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,5,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getReschedulefromDropdownNum(Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.getCancelfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on cancel in Dropdown", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Doctor.driver, logger),
				"Clicking to get reasons in Cancel Alert", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Doctor.driver, logger),
				"Clicking on reasons in Cancel Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Doctor.driver, logger),
				"Clicking on Submit in Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

	}

	/***
	 * @author Bala Yaswanth
	 * @throws Exception BA + CI + Pre consulting + preconsulted - Consultation
	 */
	@Test(priority = 4, groups = { "Regression", "Appointment_Doctor" })
	public synchronized void APP_DOC_004() throws Exception {

		logger = Reports.extent.createTest("APP_DOC_004");

		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		int num;

		// Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId) - 2);

		// Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);

		// Checking in booked appointment
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking to get Appointment Dropdown", Login_Doctor.driver, logger);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getCheckInfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getCheckInfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on checkin in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Check-in to Pre Consultation

		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Checked in  Appointment", Login_Doctor.driver, logger);
		// *Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getPreConsultationfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getPreConsultationfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on Pre consultating in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// PrecConsultation to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// Appointment Dashboard to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger),
				"Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Pre Consultation to Preconsulting
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Checked in  Appointment", Login_Doctor.driver, logger);
		// *Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getPreConsultingfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getPreConsultingfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on Pre consultating in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Preconsulting to Appointment Dashboard
		// **Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver,
		// logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver,
		// logger);

		// preconsulting to All Slots Page
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Appointment Dashboard to All slots Page
		// Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver,
		// logger), "Moving to All slots page from Appointments Page",
		// Login_Doctor.driver, logger);
		// Thread.sleep(6000);

		// Preconsulting to Preconsulted
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		// num = ASpage.getPreConsultedfromDropdownNum(Login_Doctor.driver, logger);
		// Web_GeneralFunctions.click(ASpage.getPreConsultedfromDropdown(num,
		// Login_Doctor.driver, logger),"Clicking on preConsulted in Dropdown",
		// Login_Doctor.driver, logger);
		// Thread.sleep(6000);

		// preconsulted to All Slots Page
		// **Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Doctor.driver,
		// logger), Login_Doctor.driver);
		// **Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Doctor.driver,
		// logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver,
		// logger);
		// **Thread.sleep(6000);
		// generalFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver,
		// logger), "Moving to All slots page from Appointments Page",
		// Login_Doctor.driver, logger);

		// Preconsulted to Consult
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);

		num = ASpage.getConsultfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getConsultfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on consult in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		ConsultPopupCheck();
		Web_GeneralFunctions.wait(6);

		// Moving to Appointments Page
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger),
				"Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
	}

	/***
	 * @author Bala Yaswanth
	 * @throws Exception BA + Consult1 + Consult2 + Checkout - View and Print
	 */
	@Test(priority = 5, groups = { "Regression", "Appointment_Doctor" })
	public synchronized void APP_DOC_005() throws Exception {

		logger = Reports.extent.createTest("APP_DOC_005");

		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		ConsultationPage ConsPage = new ConsultationPage();

		// Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId) - 2);
		int num;
		// Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);

		// Consult with booked appointment
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,3,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);
		num = ASpage.getConsultfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getConsultfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on consult in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		ConsultPopupCheck();

		Web_GeneralFunctions.wait(6);

		// Consult to Appointments
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// Appointments to All slots page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger),
				"Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// To Consulting Again
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);
		num = ASpage.getConsultingfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getConsultingfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on consult in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Add data and Scroll and Checkout
		Web_GeneralFunctions.sendkeys(ConsPage.setNotes(Login_Doctor.driver, logger),
				"Some Random Notes for Automation", "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver,
				logger);
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(6);
		Web_GeneralFunctions.click(ConsPage.getCheckoutBtn(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		NoDiagnosisCheck();
		Web_GeneralFunctions.click(ConsPage.getCloseBtninPopup(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Billing page to Appointments
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// Appointments to All slots page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger),
				"Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// To View Again -click on CkeckedOut
		Web_GeneralFunctions.scrollToElement(
				ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		// **Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1,
		// Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown",
		// Login_Doctor.driver, logger);
		num = ASpage.getCheckedOutfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getCheckedOutfromDropdown(num, Login_Doctor.driver, logger),
				"Clicking on consult in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

		// Scroll and Print
		Web_GeneralFunctions.scrollToElement(ConsPage.getPrintAllBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ConsPage.getPrintAllBtn(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);

		// Closing the printed page
		ArrayList<String> tabs = new ArrayList<String>(Login_Doctor.driver.getWindowHandles());
		Login_Doctor.driver.switchTo().window(tabs.get(1));
		Login_Doctor.driver.close();
		tabs = new ArrayList<String>(Login_Doctor.driver.getWindowHandles());
		Login_Doctor.driver.switchTo().window(tabs.get(0));

		// Moving to Appointments Page from Checked out consultation
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger),
				"Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
	}

	public synchronized void ScrollAndBookAllSlots(String prevSlotId, String slotId) throws Exception {

		AllSlotsPage ASpage = new AllSlotsPage();

		Web_GeneralFunctions.scrollElementToCenter(
				ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), Login_Doctor.driver);

		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger),
				"Clicking on Plus Icon", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger),
				"8553406065", "sending value in search box", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(Login_Doctor.driver, logger),
				"Clicking on first available consumer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(Login_Doctor.driver, logger), "EMR Automation Testing",
				"Setting Visit Reason", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Doctor.driver, logger),
				"Clicking to Submit Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);

	}

	public synchronized void ConsultPopupCheck() throws Exception {

		CommonPage CPage = new CommonPage();
		try {
			Web_GeneralFunctions.click(CPage.getConsultationAlert(Login_Doctor.driver, logger),
					"Clicking to Checkout and start new Appointment", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println("**********************Logging**********************");
			System.out.println("Unable to Checkout and Consult in Conultation Popup");
			System.out.println("**********************Logged***********************");
		}

	}

	public synchronized void NoDiagnosisCheck() throws Exception {

		ConsultationPage ConsPage = new ConsultationPage();

		try {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(Login_Doctor.driver, logger),
					"Clicking to Checkout and start new Appointment", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println("****************Logging*************");
			System.out.println("Unable to Checkout without Diagnosis");
			System.out.println("****************Logged**************");
		}

	}

}