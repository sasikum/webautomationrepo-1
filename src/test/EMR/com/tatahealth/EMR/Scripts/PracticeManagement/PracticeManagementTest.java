
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.PracticeManagement.CaseSheetDesignerPages;
import com.tatahealth.EMR.pages.PracticeManagement.ConsultationFlowPages;
import com.tatahealth.EMR.pages.PracticeManagement.ExternalTestTemplatePages;
import com.tatahealth.EMR.pages.PracticeManagement.GeneralAdviceTemplatePages;
import com.tatahealth.EMR.pages.PracticeManagement.GoogleCalendarPages;
import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.EMR.pages.PracticeManagement.SummmaryFlowPages;
import com.tatahealth.EMR.pages.PracticeManagement.VitalMasterPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PracticeManagementTest {
	
	ExtentTest logger;
	
	@BeforeClass(alwaysRun=true,groups= {"Regression","PracticeManagement"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement";
		Login_Doctor.LoginTest();
	}
	
	@AfterClass(alwaysRun=true,groups = { "Regression","PracticeManagement"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=400)
	public synchronized void moveToPracticeManagement()throws Exception{
		
		logger = Reports.extent.createTest("EMR  verify Practice management implementation");
		PracticeManagementPages pm = new PracticeManagementPages();
		Web_GeneralFunctions.click(pm.getLeftMainMenu(Login_Doctor.driver, logger), "Click Left main menu ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getPracticeManagementMenu(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getPracticeManagementMenu(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getPracticeManagementMenu(Login_Doctor.driver, logger), "Click Practice Management menu ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getCaseSheetDesigner(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getExternalTests(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getGeneralAdvice(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getMedicalKits(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=401)
	public synchronized void validateDispalyOfPracticeManagementModules() {
		logger = Reports.extent.createTest("EMR  verify display of practice management modules");
		PracticeManagementPages pm = new PracticeManagementPages();
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getPrescription(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getExternalTests(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getMedicalKits(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getConsultationFlow(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getGoogleCalender(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getConsultationPdfDesigner(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getSummaryPdfPreference(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getVitalsMaster(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getCaseSheetDesigner(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pm.getGeneralAdvice(Login_Doctor.driver, logger)));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=402)
	public synchronized void moveFromOneModuleToAnotherAndVerify() throws Exception {
		logger = Reports.extent.createTest("EMR Practice management move from one module to another");
		PracticeManagementPages pm = new PracticeManagementPages();
		CaseSheetDesignerPages csdp = new CaseSheetDesignerPages();
		ConsultationFlowPages cfp = new ConsultationFlowPages();
		ExternalTestTemplatePages etp = new ExternalTestTemplatePages();
		GeneralAdviceTemplatePages gat = new GeneralAdviceTemplatePages();
		GoogleCalendarPages gcp = new GoogleCalendarPages();
		MedicalKitPages mkp = new MedicalKitPages();
		SummmaryFlowPages sfp = new SummmaryFlowPages();
		VitalMasterPages vmp = new VitalMasterPages();
		Web_GeneralFunctions.click(pm.getExternalTests(Login_Doctor.driver, logger), "clicking on External Tests",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, etp.getCreateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, etp.getLabTestIdSearchField(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(etp.getCreateTemplateButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(etp.getLabTestIdSearchField(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getVitalsMaster(Login_Doctor.driver, logger),"clicking on Vitals Master",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vmp.getSaveButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(vmp.getSaveButton(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getConsultationFlow(Login_Doctor.driver, logger), "click on consultation flow", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cfp.getPreExaminationModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfp.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfp.getPreExaminationModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfp.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfp.getVitalModule(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getSummaryPdfPreference(Login_Doctor.driver, logger), "click on summary pdf", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sfp.getGeneralAdvice(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sfp.getDiagnosis(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(sfp.getGeneralAdvice(Login_Doctor.driver,logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(sfp.getDiagnosis(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(sfp.getPrescription(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getGoogleCalender(Login_Doctor.driver, logger), "click on google calender", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gcp.getConnectButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(gcp.getConnectButton(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getMedicalKits(Login_Doctor.driver, logger), "click on medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mkp.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mkp.getMedicalKitDropDown(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(mkp.getCreateKitButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mkp.getMedicalKitDropDown(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getCaseSheetDesigner(Login_Doctor.driver, logger), "click on caseSheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,csdp.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, csdp.getSelectButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(csdp.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(csdp.getSelectButton(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pm.getGeneralAdvice(Login_Doctor.driver, logger), "click on general advice", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getCreateTemplate(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(gat.getCreateTemplate(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(gat.getSaveButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(gat.getDeleteButton(Login_Doctor.driver,logger)));
	}
	
	public synchronized void clickOrgHeader()throws Exception{
		
		logger = Reports.extent.createTest("EMR Practice management");
		PracticeManagementPages pm = new PracticeManagementPages();
		AppointmentsPage ap = new AppointmentsPage();
		Web_GeneralFunctions.click(pm.getOrgHeader(Login_Doctor.driver, logger), "Click org  header", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
	}
	
	public synchronized void consult() throws Exception {
		
		logger = Reports.extent.createTest("EMR Consultation");
		PracticeManagementPages pmp = new PracticeManagementPages();
		ConsultationTest consult = new ConsultationTest();
		if(pmp.isappointmentDisplayed(Login_Doctor.driver, logger)) {
			consult.startConsultation();
		}else {
			Web_GeneralFunctions.click(pmp.clickOption(Login_Doctor.driver, logger), "click option", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(2);
			Web_GeneralFunctions.click(pmp.selectConsulting(Login_Doctor.driver, logger), "Leads to consultation page", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(2);
		}
	}
	
}

