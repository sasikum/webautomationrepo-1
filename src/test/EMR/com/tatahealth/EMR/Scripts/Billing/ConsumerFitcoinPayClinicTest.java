package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerFitcoinPayClinicTest {
	
	
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerFitcoinPayClinicTest";
		  BillingTest.doctorName="Kasu";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	
	/*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
	/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
	 * Biiling_C_P_47-Biiling_C_P_48
	 */
	  
	  /*Test case:Billing_C_PP_8,Biiling_C_P_5*/
	  	@Test(priority=110,groups = {"G3"})
	   	public void consumerFPcPaymentCashWalletEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash Wallet");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Wallet, Rewards";
			String paidStatus="Cash, Wallet";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
	   	 
	   	}
	  	
	  	@Test(priority=111,groups = {"G3"})
	   	public void consumerFPcPaymentCashDebitCardEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash Debit Card");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Debit card, Rewards";
			String paidStatus="Cash, Debit card";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
	   	 
	   	}
	  	@Test(priority=112,groups = {"G1"})
	   	public void consumerFPcPaymentCashCreditCardEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash Credit Card");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Credit card, Rewards";
			String paidStatus="Cash, Credit card";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
	   	 
	   	}
	  	
	  	@Test(priority=113,groups = {"G2"})
	   	public void consumerFPcPaymentCashEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Rewards";
			String paidStatus="Cash";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
	   	 
	   	}
	  	
	  	
	  	@Test(priority=114,groups = {"G1"})
	   	public void consumerFPcPaymentCreditCardEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Credit Card");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Credit card, Rewards";
			String paidStatus="Credit card";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
	   	 
	   	}
	  	
		@Test(priority=115,groups = {"G2"})
	   	public void consumerFPcPaymentWalletEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Wallet");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Wallet, Rewards";
			String paidStatus="Wallet";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageValidation();
	   		 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
	   	 
	   	}
		
		
		//No Service 
		
		@Test(priority=116,groups = {"G2"})
	   	public void consumerFPcPaymentCashWalletNoServiceEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash Wallet No Service");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Wallet, Rewards";
			String paidStatus="Cash, Wallet";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageNoServiceValidation();
	   		 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletNoServicePdfValidation();
	   	 
	   	}
	  	
	  	@Test(priority=117,groups = {"G1"})
	   	public void consumerFPcPaymentCashDebitCardNoServiceEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash Debit Card No Service");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Debit card, Rewards";
			String paidStatus="Cash, Debit card";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageNoServiceValidation();
	   		 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardNoServicePdfValidation();
	   	 
	   	}
	  	
	  	@Test(priority=118,groups = {"G3"})
	   	public void consumerFPcPaymentCashNoServiceEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Cash No Service");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Cash, Rewards";
			String paidStatus="Cash";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageNoServiceValidation();
	   		 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashNoServicePdfValidation();
	   	 
	   	}
	  	
	  	
	  	@Test(priority=119,groups = {"G2"})
	   	public void consumerFPcPaymentCreditCardNoServiceEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Credit Card No Service");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Credit card, Rewards";
			String paidStatus="Credit card";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageNoServiceValidation();
	   		 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardNoServicePdfValidation();
	   	 
	   	}
	  	
		@Test(priority=120,groups = {"G1"})
	   	public void consumerFPcPaymentWalletNoServiceEmrValidation()throws Exception
	   	{
	   		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Payment Emr Validation with Wallet No Service");
	   		consumer.consumerBookAppointment("Fit Coin + Pay At Clinic");
	   		String paymentCheck="Wallet, Rewards";
			String paidStatus="Wallet";
	   		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
	   		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
	   		 appt.appointmentPaymentModeFPcStatusBooked();
	   		 appt.bookedAppointmentOptionValidation();
	   		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
	   		 consumer.consumerFPcBillingPageNoServiceValidation();
	   		 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	   	  appt.appointmentPaymentModeFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  	  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletNoServicePdfValidation();
	   	 
	   	}
		

}
