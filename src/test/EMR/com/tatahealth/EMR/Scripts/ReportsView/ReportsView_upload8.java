package com.tatahealth.EMR.Scripts.ReportsView;

import java.util.Random;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload8 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	SweetAlertPage swaPage = new SweetAlertPage();
	

	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	public synchronized void getCurrnetPageURL() {
		Login_Doctor.driver.getCurrentUrl();
	}
	@Test(priority = 64, groups = { "Regression", "ReportView" }, enabled = false)
	public void checkReportExcelFileUpload() throws Exception {
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");
		logger = Reports.extent.createTest("To check whether user can able to upload word and excel file");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		try {
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\ExcelReport.xlsx");
			String actualAlertMessage = report.getUploadAlertText(Login_Doctor.driver, logger).getText();
			Assert.assertEquals(actualAlertMessage, "This file type is not allowed!");
			report.getFileUpoadAcceptBtn(Login_Doctor.driver, logger).click();
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 65, groups = { "Regression", "ReportView" }, enabled = false)
	public void checkUploadPdfReport() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to upload pdf file in upload reports pop up");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		try {
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\PDFReport.pdf");
			Web_GeneralFunctions.wait(2);
			report.getUploadBtn(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(3);
			String successMessage = report.getUploadSuccessMessage(Login_Doctor.driver, logger).getText();
			Assert.assertEquals(successMessage, "Report uploaded successfully");
			Web_GeneralFunctions.wait(3);
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 66, groups = { "Regression", "ReportView" })
	public void verifyMessageWithoutAttachment() throws Exception {
		logger = Reports.extent.createTest("To check when user clicks on upload button with empty file");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		try {
			report.getUploadBtn(Login_Doctor.driver, logger).click();
			String successMessage = report.getUploadMessageWithoutAttachment(Login_Doctor.driver, logger).getText();
			Assert.assertEquals(successMessage, "Please select file to upload");
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 67, groups = { "Regression", "ReportView" }, enabled = false)
	public void verifyChangeUploadedFile() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to change the file after upload");
		Login_Doctor.driver.get("https://emrcstg.tatahealth.com/reports");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		try {
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\Report.jpeg");
			Web_GeneralFunctions.wait(5);
			report.getChangeFileBtn(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\Report2.jpeg");
			report.getUploadBtn(Login_Doctor.driver, logger).click();
			String successMessage = report.getUploadSuccessMessage(Login_Doctor.driver, logger).getText();
			Assert.assertEquals(successMessage, "Report uploaded successfully");
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 68, groups = { "Regression", "ReportView" }, enabled = false)
	public void reportVerifyClickingOnRemoveBtn() throws Exception {
		logger = Reports.extent.createTest("To check when user click on remove button in pop up");
		Login_Doctor.driver.get("https://emrcstg.tatahealth.com/reports");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		report.getSelectFileBtn(Login_Doctor.driver, logger).click();
		try {
			Web_GeneralFunctions.uploadFile("\\src\\test\\resources\\Report.jpeg");
			report.getRemoveFileBtn(Login_Doctor.driver, logger).click();
			Assert.assertTrue(report.getSelectFileBtn(Login_Doctor.driver, logger).isDisplayed(), "Remove button click and select button displyed");
			report.getFileUpoadAcceptBtn(Login_Doctor.driver, logger).click();
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 69, groups = { "Regression", "ReportView" })
	public void verifyClickingOnUploadedReport() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to view uploaded file in pop up");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		String mainWindow = Login_Doctor.driver.getWindowHandle();
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		try {
			report.getUploadedReport(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			Login_Doctor.driver.switchTo().window(mainWindow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test(priority = 70, groups = { "Regression", "ReportView" }, enabled = false)
	public void verifyDeletionOfUploadedFile() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to delete uploaded file in pop up");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getUploadReportLink(Login_Doctor.driver, logger).click();
		try {
			report.getUploadedReportClose(Login_Doctor.driver, logger).click();	
			report.getUploadedReportCloseConfirm(Login_Doctor.driver, logger).click();
			report.getCloseIconUploadReport(Login_Doctor.driver, logger).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
