package com.tatahealth.EMR.Scripts.UserSignUp;

  
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.EMR.pages.UserProfile.UserProfilePage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class SignUp extends UserProfilePage


{
	

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		//Login_Doctor.LoginTestwithDiffrentUser("SonalinB");
		//Login_Doctor.LoginTestwithDiffrentUser("Digitizer");

		Login_Doctor.LoginTestwithDiffrentUser("Doctor");
		

		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "Appointment_Doctor" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	public static WebDriver driver;
	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();
	OTP otp =new OTP();

	

	// singup code
	@Test(priority = 2, groups = { "Regression", "SignUp" })
	public synchronized void APP_DOC_003() throws Exception {

		logger = Reports.extent.createTest("EMR_CS_3");
		Thread.sleep(1000);
		Web_GeneralFunctions.click(commonPage.getlogOutImg(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getSignUpLink(Login_Doctor.driver, logger), "Clicked on  getSignUpLink",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getIAgreeCbx(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getProceedBtn(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(loginPage.getSignupClinicUserIdTbx(Login_Doctor.driver, logger), "883",
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getSignupOrgIdTbx(Login_Doctor.driver, logger), "1", "filled  ORGID",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getvalidateSignUpBtn(Login_Doctor.driver, logger), "Clicked on  submit",
				Login_Doctor.driver, logger);
		Thread.sleep(100);

	 
		Web_GeneralFunctions.sendkeys(loginPage.getuserLoginNameTbx(Login_Doctor.driver, logger), "Sonalin",
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getverifyLoginNameBtn(Login_Doctor.driver, logger), "Clicked on  verifyLoginNameBtn",
				Login_Doctor.driver, logger);
		
		Thread.sleep(100);
		String otp1 = otp.getStagingOTP("4546364857");
		System.out.println("............"+otp1);
		Web_GeneralFunctions.sendkeys(loginPage.getSignupOtpTbx(Login_Doctor.driver, logger), otp1,
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		 
		 
		// Web_GeneralFunctions.sendkeys(loginPage.getuserLoginNameTbx(Login_Doctor.driver,
		// logger), "auto", "filled userLoginName", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.click(loginPage.getverifyLoginNameBtn(Login_Doctor.driver,
		// logger),"Clicked on verifyLoginNameBtn", Login_Doctor.driver, logger);

	}

}
